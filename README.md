# CONTROL DE USUARIOS

La presente tiene como finalidad brindar soluciones de CONTROL DE USUARIOS requerimientos:

- docker
- docker-compose



## DESPLIEGUE

Para su despliegue, una vez cumplimentados los requerimientos antes mencionado, se necesita seguir los siguientes pasos:

- Clonar el repositorio:

```
git clone https://gitlab.com/yoleito/lemp_users_control.git
```

- Ir al root del proyecto

```
cd lemp_users_control

```

- Ejecutar comando docker-compose para levantar el proyecto. 

Nota: si utiliza SO WIN o IOS, verifique que en docker desktop tenga agregado el root path de la ubicación del .yml en Settings -> Resources -> FILE SHARING

```
docker-compose up -d
```

- Ver en navegador el proyecto

En entornos locales el proyecto esta configurado para correr en el http://localhost:8040/login.php

- Credenciales default

Correo: secret@admin.com
Clave: 123456

