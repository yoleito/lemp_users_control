<?php namespace Views;


if (!isset($_SESSION['id'])) {
    header("location: " . BASE_PATH . "logout.php");
}

if ($_SESSION['usuario_nivel'] == 2) {
    header("location: " . BASE_PATH . "logout.php?error=5");
}

use Controllers\profilesController as profilesController;

$profile = new profilesController();


$roles = $profile->listarRoles();



use Views\Template as Template;
	
$template = new Template();


?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-lg-8">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0">Nuevo Usuarios</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a class="btn bg-gradient-dark mb-0" href="/index.php"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;volver</a>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-5">
                    <div class="row">
                        <div class="col-12">
                            <form role="form addUser" name="formAddUser" method="post" action="" autocomplete="off">
                                <div class="mb-3">
                                    <input type="text" id="user" name="user" class="form-control" placeholder="Usuario" aria-label="Usuario" aria-describedby="user-addon" required>
                                </div>
                                <div class="mb-3">
                                    <input type="email" id="mail" name="mail" class="form-control" placeholder="correo" aria-label="Correo" aria-describedby="email-addon" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                                </div>
                                <div class="mb-3">
                                    <input type="password" id="pass" name="pass" class="form-control" placeholder="clave" aria-label="Clave" minlength="6" aria-describedby="password-addon" required>
                                </div>
                                <div class="mb-3">
                                    <select class="form-select" id="rol" name="rol" aria-label="select rol" aria-describedby="select-addon">
                                    
                                        <option disabled selected>Seleccione un rol</option>
                                        <?php foreach ($roles as $rol) { ?>
                                        <option value="<?php echo $rol['id'] ?>">
                                            <?php echo strtoupper($rol['rol']); ?>
                                        </option>
                                        <?php } ?>
                                    
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>