<?php namespace Views;


if (!isset($_SESSION['id'])) {
    header("location: " . API . "login.php?mensaje=Te saque de index profile");
}


use Controllers\profilesController as profilesController;

$profile = new profilesController();

$usuarios = $profile->usuarios($_SESSION['usuario_id']);

use Views\Template as Template;
	
$template = new Template();


?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0">Listado de Usuarios</h6>
                        </div>
                        <div class="col-6 text-end">
                        <?php if ($_SESSION['usuario_nivel'] != 2) { ?>
                            <a class="btn bg-gradient-dark mb-0" href="/profiles/add"><i class="fas fa-plus"></i>&nbsp;&nbsp;Nuevo Usuario</a>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Usuario</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Rol</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Estado</th>
                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(mysqli_num_rows($usuarios) < 1) { ?>
                            <tr>
                                <td colspan="4">
                                    <p class="text-center">No hay datos.</p>
                                </td>
                            </tr>
                        <?php }else{ ?>
                            <?php foreach ($usuarios as $usuario) { ?>
                            <tr>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm"><?php echo $usuario['user']?></h6>
                                            <p class="text-xs text-secondary mb-0"><?php echo $usuario['mail']?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0"><?php echo $usuario['nombre_rol']?></p>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <?php if ($usuario['activo'] == 1) { ?>
                                        <span class="badge badge-sm bg-gradient-success">activo</span>
                                    <?php }else{ ?>
                                        <span class="badge badge-sm bg-gradient-danger">inactivo</span>
                                    <?php } ?>
                                </td>
                                <td class="align-middle text-end">
                                <?php if ($_SESSION['usuario_nivel'] != 2) { ?>
                                    <a href="profiles/delete/<?php echo $usuario['id'] ?>" class="text-secondary font-weight-bold text-xs px-2" data-toggle="tooltip" data-original-title="Borrar Usuario">
                                        <i class="fa fa-trash me-sm-1"></i>
                                        <span class="d-sm-inline d-none">Eliminar</span>
                                    </a>
                                    <a href="profiles/edit/<?php echo $usuario['id'] ?>" class="text-secondary font-weight-bold text-xs px-2" data-toggle="tooltip" data-original-title="Editar Usuario">
                                        <i class="fa fa-pencil-alt me-sm-1"></i>
                                        <span class="d-sm-inline d-none">Editar</span>
                                    </a>
                                    <?php if ($usuario['activo'] == 1) { ?>
                                        <a href="profiles/disable/<?php echo $usuario['id'] ?>" class="text-secondary font-weight-bold text-xs px-2" data-toggle="tooltip" data-original-title="Desactivar Usuario">
                                            <i class="fa fa-user-slash me-sm-1"></i>
                                            <span class="d-sm-inline d-none">Desactivar</span>
                                        </a>
                                    <?php }else{ ?>
                                        <a href="profiles/enable/<?php echo $usuario['id'] ?>" class="text-secondary font-weight-bold text-xs px-2" data-toggle="tooltip" data-original-title="Activar Usuario">
                                            <i class="fa fa-user me-sm-1"></i>
                                            <span class="d-sm-inline d-none">Activar</span>
                                        </a>
                                    <?php } ?>
                                <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

