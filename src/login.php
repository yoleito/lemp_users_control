<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <title>
    Control de Usuarios
  </title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <link id="pagestyle" href="./assets/css/style.css" rel="stylesheet" />
</head>

<body class="">
  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
              <?php if (isset($_SERVER['HTTP_REFERER']) && isset($_GET['mensaje'])) { ?>
              <div class="alert alert-danger text-light" role="alert">
              <strong><?php echo($_GET['mensaje']); ?></strong>
              </div>
              <?php }elseif(!isset($_SERVER['HTTP_REFERER']) && isset($_GET['mensaje'])){ ?>
                <div class="alert alert-danger text-light" role="alert">
                  <strong>Fea actitud tocar la url buscando vulnerabilidad, ;)</strong>
                </div>
              <?php } ?>
              <div class="card card-plain mt-2">
                <div class="card-header pb-0 text-left bg-transparent">
                  <h3 class="font-weight-bolder text-info text-gradient">¡Bienvenido!</h3>
                  <p class="mb-0">Ingrese su correo y clave</p>
                </div>
                <div class="card-body">
                  <form role="form" name="formLogin" method="post" action="index.php" autocomplete="off"  onsubmit="return validar()">
                    <label>Correo</label>
                    <div class="mb-3">
                      <input type="email" id="email" name="email" class="form-control" placeholder="correo" aria-label="Correo" aria-describedby="email-addon" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                    </div>
                    <label>Clave</label>
                    <div class="mb-3">
                      <input type="password" id="password" name="password" class="form-control" placeholder="clave" aria-label="Clave" aria-describedby="password-addon" required>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Ingresar</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                    <p class="mb-4 text-sm mx-auto">
                    Si no posee cuenta, debe solicitarla al administrador.
                    </p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  </main>
  <script type="text/javascript">
    function validar() {
      let email = document.forms['formLogin']['email'].value;
      let password = document.forms['formLogin']['password'].value;
      if (email == '' || password == '') {
        alert('Debe completar todos los campos.')
        return false
      }
    }
  </script>
</body>

</html>