<?php

    define('DS', DIRECTORY_SEPARATOR);
    define('ROOT', realpath(dirname(__FILE__)) . DS);
    define('BASE_PATH', "http://localhost:8040/");
    
    require_once "Config/Autoload.php";

	
	
	Config\Autoload::run();
	
    if ((isset($_POST['email']) and $_POST['email'] !== '') and (isset($_POST['password']) and $_POST['password'] !== '')) {
        
        include_once('Controllers/usersController.php');
        $usersController = New Controllers\usersController;

		$verify = $usersController->verificaMail($_POST['email'], $_POST['password']);

    } else {
        $usuarios_sesion="autentificator";
		// usamos la sesion de nombre definido.
		session_name($usuarios_sesion);
		// Iniciamos el uso de sesiones
		session_cache_limiter('nocache,private');
		session_start();

		

		unset($_POST['email']);

		unset($_POST['password']);

		//define('USER', $_SESSION['usuario_nombre']);
    }

    if (!isset($_SESSION['usuario_id']) || !isset($_SESSION['usuario_nombre']) || !isset($_SESSION['id'])){
		// Borramos la sesion creada por el inicio de session anterior


		unset($_POST['email']);

		unset($_POST['password']);

		session_destroy();
		header ("Location: " . BASE_PATH . "login.php");
		exit;
	}
    
    require_once "Views/template.php";
	
	
	Config\Enrutador::run(new Config\Request());
?>