<?php namespace Config;


	class Request{

		private $controlador;
		private $metodo;
		private $argumento;


		public function __construct(){
			
			if (isset($_GET['url'])) {
				
				$ruta = filter_input(INPUT_GET,'url', FILTER_SANITIZE_URL);
				$ruta = explode("/", $ruta);
				$ruta = array_filter($ruta);
				
				if (isset($ruta[0]) && $ruta[0] == "index.php") {
					$this->controlador = "profiles";
				}else{
					$this->controlador = strtolower(array_shift($ruta));
				}

				
				$this->metodo = strtolower(array_shift($ruta));

				if (!$this->metodo) {
					$this->metodo = "index";//metodo por defecto
				}

				
				$this->argumento = $ruta;
				
			}else{
				
				//controlador y metodo por defecto cuando inicia en index.php
				$this->controlador = "profiles";
				$this->metodo = "index";
			}
			
		}

		public function getControlador(){
			return $this->controlador;
		}

		public function getMetodo(){
			return $this->metodo;
		}

		public function getArgumento(){
			return $this->argumento;
		}


	}



?>