<?php namespace Models;

	
	class Profile {

		private $id;

		private $user;

		private $mail;

		private $pass;

		private $rol_id;

		private $firts;

		private $activo;

		private $var;
		
		private $con;

		public function __construct(){
			$this->con = new Conexion();
		}

		public function set($atributo, $contenido){
			$this->$atributo = $contenido;
		}

		public function get($atributo){
			return $this->$atributo;
		}

		public function listarUsers(){
			$sql = "SELECT users.id, users.user, users.mail, rol.rol as nombre_rol, users.activo
					FROM users
					INNER JOIN rol ON users.rol_id = rol.id
					WHERE users.rol_id != 1 and users.id != '{$this->id}'
					ORDER BY users.user";
			$datos =  $this->con->consultaRetorno($sql);
			return $datos;
		}

		public function disableUser(){
			$sql = "UPDATE users SET activo = 0
					WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function enableUser(){
			$sql = "UPDATE users SET activo = 1
					WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function deleteUser(){
			$sql = "DELETE FROM users WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function createUser(){
			$sql = "INSERT INTO users (id, user, mail, pass, rol_id, firts, activo)
					VALUES (null, '{$this->user}', '{$this->mail}', '{$this->pass}', '{$this->rol_id}', '{$this->firts}', '{$this->activo}')";
			$this->con->consultaSimple($sql);
		}

		public function editUser(){
			$sql = "UPDATE users SET user='{$this->user}', mail='{$this->mail}', rol_id='{$this->rol_id}'
					WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function viewUser(){
			$sql = "SELECT users.id, users.user, users.rol_id, users.mail, rol.rol as rol
					FROM users
					INNER JOIN rol ON users.rol_id = rol.id
					WHERE users.rol_id != 1 and users.id = '{$this->id}'";
			$datos =  $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_assoc($datos);
			return $row;
		}
		
		public function roles(){
			$sql = "SELECT id, rol 
					FROM rol
					WHERE id != 1 and activo != 0 
					ORDER BY rol asc";
			$datos =  $this->con->consultaRetorno($sql);
			return $datos;
		}
		
		public function resetKey(){
			$sql = "UPDATE users SET pass = '{$this->var}', firts = 0
					WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function changePass(){
			$sql = "UPDATE users SET pass = '{$this->var}', firts = 1
					WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function estadoPass(){
			$sql = "SELECT firts 
					FROM users
					WHERE id = '{$this->id}'";
			$datos =  $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_assoc($datos);
			return $row;
		}

		public function allDisabled(){
			$sql = "SELECT users.id, users.user, rol.rol as rol
					FROM users
					INNER JOIN rol ON users.rol_id = rol.id
					WHERE users.rol_id != 1 and users.activo = 0
					ORDER BY users.id";
			$datos =  $this->con->consultaRetorno($sql);
			
			return $datos;
		}

		public function consultaPass(){
			$sql="SELECT id, pass FROM users WHERE id='{$this->var}'";
			$datos = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_assoc($datos);
			return $row;
		}

		public function verifyActive(){
			$sql="SELECT activo FROM users WHERE id='{$this->id}'";
			$datos = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_assoc($datos);
			return $row;
		}
		

		
	}



?>