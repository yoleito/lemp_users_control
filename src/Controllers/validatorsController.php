<?php namespace Controllers;

	
	
	
	class validatorsController {

		public $sesLog = "para_logeo";
		public $sesGra = "autentificator";
		public $api = "http://oscaraguad.dns.mindef/"; 

		public function texto_aleatorio ($long = 5, $letras_min = true, $letras_max = true, $num = true) {
			$salt = $letras_min?'abchefghknpqrstuvwxyz':'';
			$salt .= $letras_max?'ACDEFHKNPRSTUVWXYZ':'';
			$salt .= $num?(strlen($salt)?'2345679':'0123456789'):'';
			
			if (strlen($salt) == 0) {
				return '';
			}
				
				$i = 0;	
				$str = '';
				
				srand((double)microtime()*1000000);

				while ($i < $long) {
					$num = rand(0, strlen($salt)-1);
					$str .= substr($salt, $num, 1);		
					$i++;
				}
				return $str;
		}

		//valida que sean solo numeros
		public function soloNumeros($laCadena) {
		    $carsValidos = "0123456789";
		    for ($i=0; $i<strlen($laCadena); $i++) {
		      if (strpos($carsValidos, substr($laCadena,$i,1))===false) {
		         return false; 
		      }
		    }
		   if($laCadena > 99999999) {//verifica que el numero no sea mas grande
				return false;
			}else{
		    return true;
			}
		} 
		
		//pasa cadena a caracteres ascii
		public function limpioDatos($laCadena) {
			for($i=0;$i<strlen($laCadena);$i++){ 
    			$dato.="&#".ord($laCadena[$i]).";"; 
			} 
	
			return $dato;
		} 
		/*************************************************************************/
  		//elimina caracteres especiales
		public function elim_car_esp_login($val){
	
			$val = trim($val);
	        $val = str_replace("DELETE", "",$val);
			$val = str_replace("INSERT", "",$val);
	        $val = str_replace("UPDATE", "",$val);
	        $val = str_replace("DROP", "",$val);
			$val = str_replace("DUMP", "",$val);
			$val = str_replace("SELECT", "",$val);
			$val = str_replace("UNION", "",$val);
			$val = str_replace("LIKE", "",$val);
			$val = str_replace("JAVASCRIPT", "",$val);
			$val = str_replace("SCRIPT", "",$val);
			$val = str_replace("ALERT", "",$val);
			$val = str_replace("delete", "",$val);
	      	$val = str_replace("insert", "",$val);
	        $val = str_replace("update", "",$val);
	        $val = str_replace("drop", "",$val); 
			$val = str_replace("dump", "",$val);  
			$val = str_replace("select", "",$val);
			$val = str_replace("union", "",$val);
			$val = str_replace("like", "",$val);
			$val = str_replace("javascript", "",$val);
			$val = str_replace("script", "",$val);
			$val = str_replace("alert", "",$val);
			$val = str_replace("/", "",$val);
			$val = str_replace("{", "",$val);
			$val = str_replace("}", "",$val);
			$val = str_replace("[", "",$val);
			$val = str_replace("]", "",$val);
			$val = str_replace("*", "",$val);
			$val = str_replace("\"", "",$val);
			$val = str_replace("\\", "",$val);
			$val = str_replace("'", "",$val);
			$val = str_replace("‘", "",$val);
			$val = str_replace("’", "",$val);
			$val = str_replace("&", "",$val);
			$val = str_replace("%", "",$val);
			$val = str_replace("^", "",$val);
			$val = str_replace("", "",$val);
			$val = str_replace("“", "",$val);
			$val = str_replace("”", "",$val);
			$val = str_replace("–", "",$val);
			$val = str_replace("–", "",$val);
			$val = str_replace("<", "",$val);
			$val = str_replace(">", "",$val);
	        $val = trim($val);             
	        return $val;
	    }
		/*************************************************************************/
		//cambia caracteres especiales y cadenas prohibidas por ascii
		public function elim_car_esp_total($val){
			$val = trim($val);
			//$val = strtolower($val);//paso todo a minusculas
			$val = str_replace("delete", limpioDatos("delete"),strtolower($val));
			$val = str_replace("insert", limpioDatos("insert"),$val);
			$val = str_replace("update", limpioDatos("update"),$val);
			$val = str_replace("drop", limpioDatos("drop"),$val);
			$val = str_replace("dump", limpioDatos("dump"),$val);
			$val = str_replace("copy", limpioDatos("copy"),$val);
			$val = str_replace("select", limpioDatos("select"),$val);
			$val = str_replace("while", limpioDatos("while"),$val);
			$val = str_replace("like", limpioDatos("like"),$val);
			$val = str_replace("or", limpioDatos("or"),$val);
			$val = str_replace("and", limpioDatos("and"),$val);
			$val = str_replace("if", limpioDatos("if"),$val);
			$val = str_replace("join", limpioDatos("join"),$val);
			$val = str_replace("left", limpioDatos("left"),$val);
			$val = str_replace("inner", limpioDatos("inner"),$val);
			$val = str_replace("cross", limpioDatos("cross"),$val);
			$val = str_replace("javascript", limpioDatos("javascript"),$val);
			$val = str_replace("script", limpioDatos("script"),$val);
			$val = str_replace("alert", limpioDatos("alert"),$val);
			$val = str_replace("assign", limpioDatos("assign"),$val);
			$val = str_replace("cleartimeout", limpioDatos("cleartimeout"),$val);
			$val = str_replace("constructor", limpioDatos("constructor"),$val);
			$val = str_replace("document", limpioDatos("document"),$val);
			$val = str_replace("encodeuri", limpioDatos("encodeuri"),$val);
			$val = str_replace("focus", limpioDatos("focus"),$val);
			$val = str_replace("innerwidth", limpioDatos("innerwidth"),$val);
			$val = str_replace("mimetypes", limpioDatos("mimetypes"),$val);
			$val = str_replace("hidden", limpioDatos("hidden"),$val);
			$val = str_replace("open", limpioDatos("open"),$val);
			$val = str_replace("packages", limpioDatos("packages"),$val);
			$val = str_replace("parseint", limpioDatos("parseint"),$val);
			$val = str_replace("propertyisenum", limpioDatos("propertyisenum"),$val);
			$val = str_replace("scroll", limpioDatos("scroll"),$val);
			$val = str_replace("settimeout", limpioDatos("settimeout"),$val);
			$val = str_replace("textarea", limpioDatos("textarea"),$val);
			$val = str_replace("all", limpioDatos("all"),$val);
			$val = str_replace("blur", limpioDatos("blur"),$val);
			$val = str_replace("clientinformation", limpioDatos("clientinformation"),$val);
			$val = str_replace("crypto", limpioDatos("crypto"),$val);
			$val = str_replace("element", limpioDatos("element"),$val);
			$val = str_replace("encodeuricomponent", limpioDatos("encodeuricomponent"),$val);
			$val = str_replace("form", limpioDatos("form"),$val);
			$val = str_replace("layer", limpioDatos("layer"),$val);
			$val = str_replace("navigate", limpioDatos("navigate"),$val);
			$val = str_replace("history", limpioDatos("history"),$val);
			$val = str_replace("opener", limpioDatos("opener"),$val);
			$val = str_replace("pagexoffset", limpioDatos("pagexoffset"),$val);
			$val = str_replace("password", limpioDatos("password"),$val);
			$val = str_replace("radio", limpioDatos("radio"),$val);
			$val = str_replace("secure", limpioDatos("secure"),$val);
			$val = str_replace("status", limpioDatos("status"),$val);
			$val = str_replace("top", limpioDatos("top"),$val);
			$val = str_replace("anchor", limpioDatos("anchor"),$val);
			$val = str_replace("button", limpioDatos("button"),$val);
			$val = str_replace("close", limpioDatos("close"),$val);
			$val = str_replace("decodeuri", limpioDatos("decodeuri"),$val);
			$val = str_replace("elements", limpioDatos("elements"),$val);
			$val = str_replace("escape", limpioDatos("escape"),$val);
			$val = str_replace("forms", limpioDatos("forms"),$val);
			$val = str_replace("layers", limpioDatos("layers"),$val);
			$val = str_replace("navigator", limpioDatos("navigator"),$val);
			$val = str_replace("image", limpioDatos("image"),$val);
			$val = str_replace("option", limpioDatos("option"),$val);
			$val = str_replace("pageyoffset", limpioDatos("pageyoffset"),$val);
			$val = str_replace("pkcs11", limpioDatos("pkcs11"),$val);
			$val = str_replace("reset", limpioDatos("reset"),$val);
			$val = str_replace("submit", limpioDatos("submit"),$val);
			$val = str_replace("unescape", limpioDatos("unescape"),$val);
			$val = str_replace("anchors", limpioDatos("anchors"),$val);
			$val = str_replace("checkbox", limpioDatos("checkbox"),$val);
			$val = str_replace("closed", limpioDatos("closed"),$val);
			$val = str_replace("decodeuricomponent", limpioDatos("decodeuricomponent"),$val);
			$val = str_replace("embed", limpioDatos("embed"),$val);
			$val = str_replace("event", limpioDatos("event"),$val);
			$val = str_replace("frame", limpioDatos("frame"),$val);
			$val = str_replace("link", limpioDatos("link"),$val);
			$val = str_replace("frames", limpioDatos("frames"),$val);
			$val = str_replace("images", limpioDatos("images"),$val);
			$val = str_replace("outerheight", limpioDatos("outerheight"),$val);
			$val = str_replace("parent", limpioDatos("parent"),$val);
			$val = str_replace("plugin", limpioDatos("plugin"),$val);
			$val = str_replace("screenx", limpioDatos("screenx"),$val);
			$val = str_replace("self", limpioDatos("self"),$val);
			$val = str_replace("taint", limpioDatos("taint"),$val);
			$val = str_replace("untaint", limpioDatos("untaint"),$val);
			$val = str_replace("clearinterval", limpioDatos("clearinterval"),$val);
			$val = str_replace("confirm", limpioDatos("confirm"),$val);
			$val = str_replace("defaultstatus", limpioDatos("defaultstatus"),$val);
			$val = str_replace("embeds", limpioDatos("embeds"),$val);
			$val = str_replace("fileupload", limpioDatos("fileupload"),$val);
			$val = str_replace("innerheight", limpioDatos("innerheight"),$val);
			$val = str_replace("location", limpioDatos("location"),$val);
			$val = str_replace("framerate", limpioDatos("framerate"),$val);
			$val = str_replace("offscreenbuffering", limpioDatos("offscreenbuffering"),$val);
			$val = str_replace("outerwidth", limpioDatos("outerwidth"),$val);
			$val = str_replace("parsefloat", limpioDatos("parsefloat"),$val);
			$val = str_replace("prompt", limpioDatos("prompt"),$val);
			$val = str_replace("screeny", limpioDatos("screeny"),$val);
			$val = str_replace("setinterval", limpioDatos("setinterval"),$val);
			$val = str_replace("text", limpioDatos("text"),$val);
			$val = str_replace("window", limpioDatos("window"),$val);
			$val = str_replace("onblur", limpioDatos("onblur"),$val);
			$val = str_replace("onkeydown", limpioDatos("onkeydown"),$val);
			$val = str_replace("onload", limpioDatos("onload"),$val);
			$val = str_replace("onclick", limpioDatos("onclick"),$val);
			$val = str_replace("onkeypress", limpioDatos("onkeypress"),$val);
			$val = str_replace("onmouseup", limpioDatos("onmouseup"),$val);
			$val = str_replace("onerror", limpioDatos("onerror"),$val);
			$val = str_replace("onkeyup", limpioDatos("onkeyup"),$val);
			$val = str_replace("onmousedown", limpioDatos("onmousedown"),$val);
			$val = str_replace("onfocus", limpioDatos("onfocus"),$val);
			$val = str_replace("onmouseover", limpioDatos("onmouseover"),$val);
			$val = str_replace("onsubmit", limpioDatos("onsubmit"),$val);
			$val = str_replace("getclass", limpioDatos("getclass"),$val);
			$val = str_replace("java", limpioDatos("java"),$val);
			$val = str_replace("javaarray", limpioDatos("javaarray"),$val);
			$val = str_replace("javaclass", limpioDatos("javaclass"),$val);
			$val = str_replace("javaobject", limpioDatos("javaobject"),$val);
			$val = str_replace("javapackage", limpioDatos("javapackage"),$val);
			$val = str_replace("array", limpioDatos("array"),$val);
			$val = str_replace("infinity", limpioDatos("infinity"),$val);
			$val = str_replace("math", limpioDatos("math"),$val);
			$val = str_replace("prototype", limpioDatos("prototype"),$val);
			$val = str_replace("date", limpioDatos("date"),$val);
			$val = str_replace("isfinite", limpioDatos("isfinite"),$val);
			$val = str_replace("string", limpioDatos("string"),$val);
			$val = str_replace("eval", limpioDatos("eval"),$val);
			$val = str_replace("isnan", limpioDatos("isnan"),$val);
			$val = str_replace("name", limpioDatos("name"),$val);
			$val = str_replace("tostring", limpioDatos("tostring"),$val);
			$val = str_replace("function", limpioDatos("function"),$val);
			$val = str_replace("isprototypeof", limpioDatos("isprototypeof"),$val);
			$val = str_replace("number", limpioDatos("number"),$val);
			$val = str_replace("undefined", limpioDatos("undefined"),$val);
			$val = str_replace("hasownproperty", limpioDatos("hasownproperty"),$val);
			$val = str_replace("length", limpioDatos("length"),$val);
			$val = str_replace("object", limpioDatos("object"),$val);
			$val = str_replace("valueof", limpioDatos("valueof"),$val);
			$val = str_replace("abstract", limpioDatos("abstract"),$val);
			$val = str_replace("case", limpioDatos("case"),$val);
			$val = str_replace("continue", limpioDatos("continue"),$val);
			$val = str_replace("double", limpioDatos("double"),$val);
			$val = str_replace("extends", limpioDatos("extends"),$val);
			$val = str_replace("for", limpioDatos("for"),$val);
			$val = str_replace("import", limpioDatos("import"),$val);
			$val = str_replace("let", limpioDatos("let"),$val);
			$val = str_replace("package", limpioDatos("package"),$val);
			$val = str_replace("short", limpioDatos("short"),$val);
			$val = str_replace("this", limpioDatos("this"),$val);
			$val = str_replace("try", limpioDatos("try"),$val);
			$val = str_replace("arguments", limpioDatos("arguments"),$val);
			$val = str_replace("catch", limpioDatos("catch"),$val);
			$val = str_replace("debugger", limpioDatos("debugger"),$val);
			$val = str_replace("else", limpioDatos("else"),$val);
			$val = str_replace("false", limpioDatos("false"),$val);
			$val = str_replace("long", limpioDatos("long"),$val);
			$val = str_replace("private", limpioDatos("private"),$val);
			$val = str_replace("static", limpioDatos("static"),$val);
			$val = str_replace("throw", limpioDatos("throw"),$val);
			$val = str_replace("typeof", limpioDatos("typeof"),$val);
			$val = str_replace("with", limpioDatos("with"),$val);
			$val = str_replace("boolean", limpioDatos("boolean"),$val);
			$val = str_replace("char", limpioDatos("char"),$val);
			$val = str_replace("default", limpioDatos("default"),$val);
			$val = str_replace("enum", limpioDatos("enum"),$val);
			$val = str_replace("final", limpioDatos("final"),$val);
			$val = str_replace("goto", limpioDatos("goto"),$val);
			$val = str_replace("instanceof", limpioDatos("instanceof"),$val);
			$val = str_replace("native", limpioDatos("native"),$val);
			$val = str_replace("protected", limpioDatos("protected"),$val);
			$val = str_replace("super", limpioDatos("super"),$val);
			$val = str_replace("throws", limpioDatos("throws"),$val);
			$val = str_replace("var", limpioDatos("var"),$val);
			$val = str_replace("yield", limpioDatos("yield"),$val);
			$val = str_replace("break", limpioDatos("break"),$val);
			$val = str_replace("class", limpioDatos("class"),$val);
			$val = str_replace("finally", limpioDatos("finally"),$val);
			$val = str_replace("int", limpioDatos("int"),$val);
			$val = str_replace("new", limpioDatos("new"),$val);
			$val = str_replace("public", limpioDatos("public"),$val);
			$val = str_replace("switch", limpioDatos("switch"),$val);
			$val = str_replace("transient", limpioDatos("transient"),$val);
			$val = str_replace("void", limpioDatos("void"),$val);
			$val = str_replace("byte", limpioDatos("byte"),$val);
			$val = str_replace("const", limpioDatos("const"),$val);
			$val = str_replace("export", limpioDatos("export"),$val);
			$val = str_replace("float", limpioDatos("float"),$val);
			$val = str_replace("implements", limpioDatos("implements"),$val);
			$val = str_replace("interface", limpioDatos("interface"),$val);
			$val = str_replace("null", limpioDatos("null"),$val);
			$val = str_replace("return", limpioDatos("return"),$val);
			$val = str_replace("synchronized", limpioDatos("synchronized"),$val);
			$val = str_replace("true", limpioDatos("true"),$val);
			$val = str_replace("volatile", limpioDatos("volatile"),$val);
			$val = str_replace("screen", limpioDatos("screen"),$val);
			$val = str_replace("cookie", limpioDatos("cookie"),$val);
			$val = str_replace("getelementbyid", limpioDatos("getelementbyid"),$val);
			$val = str_replace("/", limpioDatos("/"),$val);
			$val = str_replace("{", limpioDatos("{"),$val);
			$val = str_replace("}", limpioDatos("}"),$val);
			$val = str_replace("[", limpioDatos("["),$val);
			$val = str_replace("]", limpioDatos("]"),$val);
			$val = str_replace("*", limpioDatos("*"),$val);
			$val = str_replace("%", limpioDatos("%"),$val);
			$val = str_replace("^", limpioDatos("^"),$val);
			$val = str_replace("\"", limpioDatos("\""),$val);
			$val = str_replace("\\", limpioDatos("\\"),$val);
			$val = str_replace("'", limpioDatos("'"),$val);
			$val = str_replace("‘", limpioDatos("‘"),$val);
			$val = str_replace("’", limpioDatos("’"),$val);
			$val = str_replace("'", limpioDatos("'"),$val);
			$val = str_replace("\&", limpioDatos("\&"),$val);
			$val = str_replace("“", limpioDatos("“"),$val);
			$val = str_replace("”", limpioDatos("”"),$val);
			$val = str_replace("-", limpioDatos("-"),$val);
			$val = str_replace("_", limpioDatos("_"),$val);
			$val = str_replace("<", limpioDatos("<"),$val);
			$val = str_replace(">", limpioDatos(">"),$val);
		    $val = trim($val);
		    return $val;
		}
		/*************************************************************************/
		//elimina caracteres especiales
		public function elim_car_esp_completo($val){

			$val = trim($val);
			$val = str_replace("delete", "&#100;&#101;&#108;&#101;&#116;&#101;",$val);
			$val = str_replace("insert", "&#105;&#110;&#115;&#101;&#114;&#116;",$val);
			$val = str_replace("update", "&#117;&#112;&#100;&#97;&#116;&#101;",$val);
			$val = str_replace("drop", "&#100;&#114;&#111;&#112;",$val);
			$val = str_replace("dump", "&#100;&#117;&#109;&#112;",$val);
			$val = str_replace("copy", "&#99;&#111;&#112;&#121;",$val);
			$val = str_replace("select", "&#115;&#101;&#108;&#101;&#99;&#116;",$val);
			$val = str_replace("while", "&#119;&#104;&#105;&#108;&#101;",$val);
			$val = str_replace("like", "&#108;&#105;&#107;&#101;",$val);
			$val = str_replace("or", "&#111;&#114;",$val);
			$val = str_replace("and", "&#97;&#110;&#100;",$val);
			$val = str_replace("if", "&#105;&#102;",$val);
			$val = str_replace("javascript", "&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;",$val);
			$val = str_replace("script", "&#115;&#99;&#114;&#105;&#112;&#116;",$val);
			$val = str_replace("alert", "&#97;&#108;&#101;&#114;&#116;",$val);
			$val = str_replace("assign", "&#97;&#115;&#115;&#105;&#103;&#110;",$val);
			$val = str_replace("cleartimeout", "&#99;&#108;&#101;&#97;&#114;&#116;&#105;&#109;&#101;&#111;&#117;&#116;",$val);
			$val = str_replace("constructor", "&#99;&#111;&#110;&#115;&#116;&#114;&#117;&#99;&#116;&#111;&#114;",$val);
			$val = str_replace("document", "&#100;&#111;&#99;&#117;&#109;&#101;&#110;&#116;",$val);
			$val = str_replace("encodeuri", "&#101;&#110;&#99;&#111;&#100;&#101;&#117;&#114;&#105;",$val);
			$val = str_replace("focus", "&#102;&#111;&#99;&#117;&#115;",$val);
			$val = str_replace("innerwidth", "&#105;&#110;&#110;&#101;&#114;&#119;&#105;&#100;&#116;&#104;",$val);
			$val = str_replace("mimetypes", "&#109;&#105;&#109;&#101;&#116;&#121;&#112;&#101;&#115;",$val);
			$val = str_replace("hidden", "&#104;&#105;&#100;&#100;&#101;&#110;",$val);
			$val = str_replace("open", "&#111;&#112;&#101;&#110;",$val);
			$val = str_replace("packages", "&#112;&#97;&#99;&#107;&#97;&#103;&#101;&#115;",$val);
			$val = str_replace("parseint", "&#112;&#97;&#114;&#115;&#101;&#105;&#110;&#116;",$val);
			$val = str_replace("propertyisenum", "&#112;&#114;&#111;&#112;&#101;&#114;&#116;&#121;&#105;&#115;&#101;&#110;&#117;&#109;",$val);
			$val = str_replace("scroll", "&#115;&#99;&#114;&#111;&#108;&#108;",$val);
			$val = str_replace("settimeout", "&#115;&#101;&#116;&#116;&#105;&#109;&#101;&#111;&#117;&#116;",$val);
			$val = str_replace("textarea", "&#116;&#101;&#120;&#116;&#97;&#114;&#101;&#97;",$val);
			$val = str_replace("all", "&#97;&#108;&#108;",$val);
			$val = str_replace("blur", "&#98;&#108;&#117;&#114;",$val);
			$val = str_replace("clientinformation", "&#99;&#108;&#105;&#101;&#110;&#116;&#105;&#110;&#102;&#111;&#114;&#109;&#97;&#116;&#105;&#111;&#110;",$val);
			$val = str_replace("crypto", "&#99;&#114;&#121;&#112;&#116;&#111;",$val);
			$val = str_replace("element", "&#101;&#108;&#101;&#109;&#101;&#110;&#116;",$val);
			$val = str_replace("encodeuricomponent", "&#101;&#110;&#99;&#111;&#100;&#101;&#117;&#114;&#105;&#99;&#111;&#109;&#112;&#111;&#110;&#101;&#110;&#116;",$val);
			$val = str_replace("form", "&#102;&#111;&#114;&#109;",$val);
			$val = str_replace("layer", "&#108;&#97;&#121;&#101;&#114;",$val);
			$val = str_replace("navigate", "&#110;&#97;&#118;&#105;&#103;&#97;&#116;&#101;",$val);
			$val = str_replace("history", "&#104;&#105;&#115;&#116;&#111;&#114;&#121;",$val);
			$val = str_replace("opener", "&#111;&#112;&#101;&#110;&#101;&#114;",$val);
			$val = str_replace("pagexoffset", "&#112;&#97;&#103;&#101;&#120;&#111;&#102;&#102;&#115;&#101;&#116;",$val);
			$val = str_replace("password", "&#112;&#97;&#115;&#115;&#119;&#111;&#114;&#100;",$val);
			$val = str_replace("radio", "&#114;&#97;&#100;&#105;&#111;",$val);
			$val = str_replace("secure", "&#115;&#101;&#99;&#117;&#114;&#101;",$val);
			$val = str_replace("status", "&#115;&#116;&#97;&#116;&#117;&#115;",$val);
			$val = str_replace("top", "&#116;&#111;&#112;",$val);
			$val = str_replace("anchor", "&#97;&#110;&#99;&#104;&#111;&#114;",$val);
			$val = str_replace("button", "&#98;&#117;&#116;&#116;&#111;&#110;",$val);
			$val = str_replace("close", "&#99;&#108;&#111;&#115;&#101;",$val);
			$val = str_replace("decodeuri", "&#100;&#101;&#99;&#111;&#100;&#101;&#117;&#114;&#105;",$val);
			$val = str_replace("elements", "&#101;&#108;&#101;&#109;&#101;&#110;&#116;&#115;",$val);
			$val = str_replace("escape", "&#101;&#115;&#99;&#97;&#112;&#101;",$val);
			$val = str_replace("forms", "&#102;&#111;&#114;&#109;&#115;",$val);
			$val = str_replace("layers", "&#108;&#97;&#121;&#101;&#114;&#115;",$val);
			$val = str_replace("navigator", "&#110;&#97;&#118;&#105;&#103;&#97;&#116;&#111;&#114;",$val);
			$val = str_replace("image", "&#105;&#109;&#97;&#103;&#101;",$val);
			$val = str_replace("option", "&#111;&#112;&#116;&#105;&#111;&#110;",$val);
			$val = str_replace("pageyoffset", "&#112;&#97;&#103;&#101;&#121;&#111;&#102;&#102;&#115;&#101;&#116;",$val);
			$val = str_replace("pkcs11", "&#112;&#107;&#99;&#115;&#49;&#49;",$val);
			$val = str_replace("reset", "&#114;&#101;&#115;&#101;&#116;",$val);
			$val = str_replace("select", "&#115;&#101;&#108;&#101;&#99;&#116;",$val);
			$val = str_replace("submit", "&#115;&#117;&#98;&#109;&#105;&#116;",$val);
			$val = str_replace("unescape", "&#117;&#110;&#101;&#115;&#99;&#97;&#112;&#101;",$val);
			$val = str_replace("anchors", "&#97;&#110;&#99;&#104;&#111;&#114;&#115;",$val);
			$val = str_replace("checkbox", "&#99;&#104;&#101;&#99;&#107;&#98;&#111;&#120;",$val);
			$val = str_replace("closed", "&#99;&#108;&#111;&#115;&#101;&#100;",$val);
			$val = str_replace("decodeuricomponent", "&#100;&#101;&#99;&#111;&#100;&#101;&#117;&#114;&#105;&#99;&#111;&#109;&#112;&#111;&#110;&#101;&#110;&#116;",$val);
			$val = str_replace("embed", "&#101;&#109;&#98;&#101;&#100;",$val);
			$val = str_replace("event", "&#101;&#118;&#101;&#110;&#116;",$val);
			$val = str_replace("frame", "&#102;&#114;&#97;&#109;&#101;",$val);
			$val = str_replace("link", "&#108;&#105;&#110;&#107;",$val);
			$val = str_replace("frames", "&#102;&#114;&#97;&#109;&#101;&#115;",$val);
			$val = str_replace("images", "&#105;&#109;&#97;&#103;&#101;&#115;",$val);
			$val = str_replace("outerheight", "&#111;&#117;&#116;&#101;&#114;&#104;&#101;&#105;&#103;&#104;&#116;",$val);
			$val = str_replace("parent", "&#112;&#97;&#114;&#101;&#110;&#116;",$val);
			$val = str_replace("plugin", "&#112;&#108;&#117;&#103;&#105;&#110;",$val);
			$val = str_replace("screenx", "&#115;&#99;&#114;&#101;&#101;&#110;&#120;",$val);
			$val = str_replace("self", "&#115;&#101;&#108;&#102;",$val);
			$val = str_replace("taint", "&#116;&#97;&#105;&#110;&#116;",$val);
			$val = str_replace("untaint", "&#117;&#110;&#116;&#97;&#105;&#110;&#116;",$val);
			$val = str_replace("area", "&#97;&#114;&#101;&#97;",$val);
			$val = str_replace("clearinterval", "&#99;&#108;&#101;&#97;&#114;&#105;&#110;&#116;&#101;&#114;&#118;&#97;&#108;",$val);
			$val = str_replace("confirm", "&#99;&#111;&#110;&#102;&#105;&#114;&#109;",$val);
			$val = str_replace("defaultstatus", "&#100;&#101;&#102;&#97;&#117;&#108;&#116;&#115;&#116;&#97;&#116;&#117;&#115;",$val);
			$val = str_replace("embeds", "&#101;&#109;&#98;&#101;&#100;&#115;",$val);
			$val = str_replace("fileupload", "&#102;&#105;&#108;&#101;&#117;&#112;&#108;&#111;&#97;&#100;",$val);
			$val = str_replace("innerheight", "&#105;&#110;&#110;&#101;&#114;&#104;&#101;&#105;&#103;&#104;&#116;",$val);
			$val = str_replace("location", "&#108;&#111;&#99;&#97;&#116;&#105;&#111;&#110;",$val);
			$val = str_replace("framerate", "&#102;&#114;&#97;&#109;&#101;&#114;&#97;&#116;&#101;",$val);
			$val = str_replace("offscreenbuffering", "&#111;&#102;&#102;&#115;&#99;&#114;&#101;&#101;&#110;&#98;&#117;&#102;&#102;&#101;&#114;&#105;&#110;&#103;",$val);
			$val = str_replace("outerwidth", "&#111;&#117;&#116;&#101;&#114;&#119;&#105;&#100;&#116;&#104;",$val);
			$val = str_replace("parsefloat", "&#112;&#97;&#114;&#115;&#101;&#102;&#108;&#111;&#97;&#116;",$val);
			$val = str_replace("prompt", "&#112;&#114;&#111;&#109;&#112;&#116;",$val);
			$val = str_replace("screeny", "&#115;&#99;&#114;&#101;&#101;&#110;&#121;",$val);
			$val = str_replace("setinterval", "&#115;&#101;&#116;&#105;&#110;&#116;&#101;&#114;&#118;&#97;&#108;",$val);
			$val = str_replace("text", "&#116;&#101;&#120;&#116;",$val);
			$val = str_replace("window", "&#119;&#105;&#110;&#100;&#111;&#119;",$val);
			$val = str_replace("onblur", "&#111;&#110;&#98;&#108;&#117;&#114;",$val);
			$val = str_replace("onkeydown", "&#111;&#110;&#107;&#101;&#121;&#100;&#111;&#119;&#110;",$val);
			$val = str_replace("onload", "&#111;&#110;&#108;&#111;&#97;&#100;",$val);
			$val = str_replace("onclick", "&#111;&#110;&#99;&#108;&#105;&#99;&#107;",$val);
			$val = str_replace("onkeypress", "&#111;&#110;&#107;&#101;&#121;&#112;&#114;&#101;&#115;&#115;",$val);
			$val = str_replace("onmouseup", "&#111;&#110;&#109;&#111;&#117;&#115;&#101;&#117;&#112;",$val);
			$val = str_replace("onerror", "&#111;&#110;&#101;&#114;&#114;&#111;&#114;",$val);
			$val = str_replace("onkeyup", "&#111;&#110;&#107;&#101;&#121;&#117;&#112;",$val);
			$val = str_replace("onmousedown", "&#111;&#110;&#109;&#111;&#117;&#115;&#101;&#100;&#111;&#119;&#110;",$val);
			$val = str_replace("onfocus", "&#111;&#110;&#102;&#111;&#99;&#117;&#115;",$val);
			$val = str_replace("onmouseover", "&#111;&#110;&#109;&#111;&#117;&#115;&#101;&#111;&#118;&#101;&#114;",$val);
			$val = str_replace("onsubmit", "&#111;&#110;&#115;&#117;&#98;&#109;&#105;&#116;",$val);
			$val = str_replace("getclass", "&#103;&#101;&#116;&#99;&#108;&#97;&#115;&#115;",$val);
			$val = str_replace("java", "&#106;&#97;&#118;&#97;",$val);
			$val = str_replace("javaarray", "&#106;&#97;&#118;&#97;&#97;&#114;&#114;&#97;&#121;",$val);
			$val = str_replace("javaclass", "&#106;&#97;&#118;&#97;&#99;&#108;&#97;&#115;&#115;",$val);
			$val = str_replace("javaobject", "&#106;&#97;&#118;&#97;&#111;&#98;&#106;&#101;&#99;&#116;",$val);
			$val = str_replace("javapackage", "&#106;&#97;&#118;&#97;&#112;&#97;&#99;&#107;&#97;&#103;&#101;",$val);
			$val = str_replace("array", "&#97;&#114;&#114;&#97;&#121;",$val);
			$val = str_replace("infinity", "&#105;&#110;&#102;&#105;&#110;&#105;&#116;&#121;",$val);
			$val = str_replace("math", "&#109;&#97;&#116;&#104;",$val);
			$val = str_replace("prototype", "&#112;&#114;&#111;&#116;&#111;&#116;&#121;&#112;&#101;",$val);
			$val = str_replace("date", "&#100;&#97;&#116;&#101;",$val);
			$val = str_replace("isfinite", "&#105;&#115;&#102;&#105;&#110;&#105;&#116;&#101;",$val);
			$val = str_replace("nan", "&#110;&#97;&#110;",$val);
			$val = str_replace("string", "&#115;&#116;&#114;&#105;&#110;&#103;",$val);
			$val = str_replace("eval", "&#101;&#118;&#97;&#108;",$val);
			$val = str_replace("isnan", "&#105;&#115;&#110;&#97;&#110;",$val);
			$val = str_replace("name", "&#110;&#97;&#109;&#101;",$val);
			$val = str_replace("tostring", "&#116;&#111;&#115;&#116;&#114;&#105;&#110;&#103;",$val);
			$val = str_replace("function", "&#102;&#117;&#110;&#99;&#116;&#105;&#111;&#110;",$val);
			$val = str_replace("isprototypeof", "&#105;&#115;&#112;&#114;&#111;&#116;&#111;&#116;&#121;&#112;&#101;&#111;&#102;",$val);
			$val = str_replace("number", "&#110;&#117;&#109;&#98;&#101;&#114;",$val);
			$val = str_replace("undefined", "&#117;&#110;&#100;&#101;&#102;&#105;&#110;&#101;&#100;",$val);
			$val = str_replace("hasownproperty", "&#104;&#97;&#115;&#111;&#119;&#110;&#112;&#114;&#111;&#112;&#101;&#114;&#116;&#121;",$val);
			$val = str_replace("length", "&#108;&#101;&#110;&#103;&#116;&#104;",$val);
			$val = str_replace("object", "&#111;&#98;&#106;&#101;&#99;&#116;",$val);
			$val = str_replace("valueof", "&#118;&#97;&#108;&#117;&#101;&#111;&#102;",$val);
			$val = str_replace("abstract", "&#97;&#98;&#115;&#116;&#114;&#97;&#99;&#116;",$val);
			$val = str_replace("case", "&#99;&#97;&#115;&#101;",$val);
			$val = str_replace("continue", "&#99;&#111;&#110;&#116;&#105;&#110;&#117;&#101;",$val);
			$val = str_replace("double", "&#100;&#111;&#117;&#98;&#108;&#101;",$val);
			$val = str_replace("extends", "&#101;&#120;&#116;&#101;&#110;&#100;&#115;",$val);
			$val = str_replace("for", "&#102;&#111;&#114;",$val);
			$val = str_replace("import", "&#105;&#109;&#112;&#111;&#114;&#116;",$val);
			$val = str_replace("let", "&#108;&#101;&#116;",$val);
			$val = str_replace("package", "&#112;&#97;&#99;&#107;&#97;&#103;&#101;",$val);
			$val = str_replace("short", "&#115;&#104;&#111;&#114;&#116;",$val);
			$val = str_replace("this", "&#116;&#104;&#105;&#115;",$val);
			$val = str_replace("try", "&#116;&#114;&#121;",$val);
			$val = str_replace("while", "&#119;&#104;&#105;&#108;&#101;",$val);
			$val = str_replace("arguments", "&#97;&#114;&#103;&#117;&#109;&#101;&#110;&#116;&#115;",$val);
			$val = str_replace("catch", "&#99;&#97;&#116;&#99;&#104;",$val);
			$val = str_replace("debugger", "&#100;&#101;&#98;&#117;&#103;&#103;&#101;&#114;",$val);
			$val = str_replace("else", "&#101;&#108;&#115;&#101;",$val);
			$val = str_replace("false", "&#102;&#97;&#108;&#115;&#101;",$val);
			$val = str_replace("function", "&#102;&#117;&#110;&#99;&#116;&#105;&#111;&#110;",$val);
			$val = str_replace("in", "&#105;&#110;",$val);
			$val = str_replace("long", "&#108;&#111;&#110;&#103;",$val);
			$val = str_replace("private", "&#112;&#114;&#105;&#118;&#97;&#116;&#101;",$val);
			$val = str_replace("static", "&#115;&#116;&#97;&#116;&#105;&#99;",$val);
			$val = str_replace("throw", "&#116;&#104;&#114;&#111;&#119;",$val);
			$val = str_replace("typeof", "&#116;&#121;&#112;&#101;&#111;&#102;",$val);
			$val = str_replace("with", "&#119;&#105;&#116;&#104;",$val);
			$val = str_replace("boolean", "&#98;&#111;&#111;&#108;&#101;&#97;&#110;",$val);
			$val = str_replace("char", "&#99;&#104;&#97;&#114;",$val);
			$val = str_replace("default", "&#100;&#101;&#102;&#97;&#117;&#108;&#116;",$val);
			$val = str_replace("enum", "&#101;&#110;&#117;&#109;",$val);
			$val = str_replace("final", "&#102;&#105;&#110;&#97;&#108;",$val);
			$val = str_replace("goto", "&#103;&#111;&#116;&#111;",$val);
			$val = str_replace("instanceof", "&#105;&#110;&#115;&#116;&#97;&#110;&#99;&#101;&#111;&#102;",$val);
			$val = str_replace("native", "&#110;&#97;&#116;&#105;&#118;&#101;",$val);
			$val = str_replace("protected", "&#112;&#114;&#111;&#116;&#101;&#99;&#116;&#101;&#100;",$val);
			$val = str_replace("super", "&#115;&#117;&#112;&#101;&#114;",$val);
			$val = str_replace("throws", "&#116;&#104;&#114;&#111;&#119;&#115;",$val);
			$val = str_replace("var", "&#118;&#97;&#114;",$val);
			$val = str_replace("yield", "&#121;&#105;&#101;&#108;&#100;",$val);
			$val = str_replace("break", "&#98;&#114;&#101;&#97;&#107;",$val);
			$val = str_replace("class", "&#99;&#108;&#97;&#115;&#115;",$val);
			$val = str_replace("delete", "&#100;&#101;&#108;&#101;&#116;&#101;",$val);
			$val = str_replace("eval", "&#101;&#118;&#97;&#108;",$val);
			$val = str_replace("finally", "&#102;&#105;&#110;&#97;&#108;&#108;&#121;",$val);
			$val = str_replace("if", "&#105;&#102;",$val);
			$val = str_replace("int", "&#105;&#110;&#116;",$val);
			$val = str_replace("new", "&#110;&#101;&#119;",$val);
			$val = str_replace("public", "&#112;&#117;&#98;&#108;&#105;&#99;",$val);
			$val = str_replace("switch", "&#115;&#119;&#105;&#116;&#99;&#104;",$val);
			$val = str_replace("transient", "&#116;&#114;&#97;&#110;&#115;&#105;&#101;&#110;&#116;",$val);
			$val = str_replace("void", "&#118;&#111;&#105;&#100;",$val);
			$val = str_replace("byte", "&#98;&#121;&#116;&#101;",$val);
			$val = str_replace("const", "&#99;&#111;&#110;&#115;&#116;",$val);
			$val = str_replace("do", "&#100;&#111;",$val);
			$val = str_replace("export", "&#101;&#120;&#112;&#111;&#114;&#116;",$val);
			$val = str_replace("float", "&#102;&#108;&#111;&#97;&#116;",$val);
			$val = str_replace("implements", "&#105;&#109;&#112;&#108;&#101;&#109;&#101;&#110;&#116;&#115;",$val);
			$val = str_replace("interface", "&#105;&#110;&#116;&#101;&#114;&#102;&#97;&#99;&#101;",$val);
			$val = str_replace("null", "&#110;&#117;&#108;&#108;",$val);
			$val = str_replace("return", "&#114;&#101;&#116;&#117;&#114;&#110;",$val);
			$val = str_replace("synchronized", "&#115;&#121;&#110;&#99;&#104;&#114;&#111;&#110;&#105;&#122;&#101;&#100;",$val);
			$val = str_replace("true", "&#116;&#114;&#117;&#101;",$val);
			$val = str_replace("volatile", "&#118;&#111;&#108;&#97;&#116;&#105;&#108;&#101;",$val);
			$val = str_replace("screen", "&#115;&#99;&#114;&#101;&#101;&#110;",$val);
			$val = str_replace("cookie", "&#99;&#111;&#111;&#107;&#105;&#101;",$val);
			$val = str_replace("getelementbyid", "&#103;&#101;&#116;&#101;&#108;&#101;&#109;&#101;&#110;&#116;&#98;&#121;&#105;&#100;",$val);
			//$val = str_replace("/", "&#47;",$val);
			$val = str_replace("{", "&#123;",$val);
			$val = str_replace("}", "&#125;",$val);
			//$val = str_replace("[", "&#91;",$val);
			//$val = str_replace("]", "&#93;",$val);
			$val = str_replace("*", "&#42;",$val);
			//$val = str_replace("%", "&#37;",$val);
			$val = str_replace("^", "&#94;",$val);
			//$val = str_replace("\"", "&#92",$val);
			//$val = str_replace("\\", "&#92",$val);
			//$val = str_replace("‘", "&#145;",$val);
			//$val = str_replace("’", "&#146;",$val);
			//$val = str_replace("'", "&#39;",$val);
			//$val = str_replace("&", "&#38;",$val);
			//$val = str_replace("“", "&#147;",$val);
			//$val = str_replace("”", "&#148;",$val);
			//$val = str_replace("-", "&#45;",$val);
			//$val = str_replace("_", "&#95;",$val);
			$val = str_replace("<", "&#60;",$val);
			$val = str_replace(">", "&#62;",$val);
			$val = trim($val); 

			return $val;
		}

		/*************************************************************************/
		//encripta la clave
		public function encripta($pass, $usuarios_md5){
			$val1 = md5($pass.$usuarios_md5);
			$val = hash_hmac("sha512",$pass,$val1);
			return $val;
		}

		public function validaError($errorenvio){
			switch ($errorenvio) {

            
            case '1':
                $message = "Verifique que los campos enten correctos y vuelva a intentarlo cuando finalice el contador";
                $multa=1;
            break;
            
            case '2':
                $message = "Verifique que los campos enten correctos y vuelva a intentarlo cuando finalice el contador";
                $multa=2;
            break;

            case '3':
                $message = "Recuerde que este tipo de ingreso tiene penalidad por tomarse como intento de vulnerabilidad del sistema, vuelva a intentarlo cuando finalice el contador";
                $multa=4; 
            break;

            case '4':
                $message = "Recuerde que este tipo de ingreso tiene penalidad por tomarse como intento de vulnerabilidad del sistema, vuelva a intentarlo cuando finalice el contador";
                $multa=4;
            break;

            case '5':
                $message = "Verifique que los campos enten correctos y vuelva a intentarlo cuando finalice el contador";
                $multa=2;
            break;
            
            case '6':
                $message = "Recuerde que este tipo de ingreso tiene penalidad por tomarse como intento de vulnerabilidad del sistema, vuelva a intentarlo cuando finalice el contador";
                $multa=20;
            break;
        	}

        	$error = array(
        		'mensaje'=>$message,
        		'multa' =>$multa);

        	return $error;	 	
		}

		public function getRealIP(){

		    if (isset($_SERVER["HTTP_CLIENT_IP"]))
		    {
		        return $_SERVER["HTTP_CLIENT_IP"];
		    }
		    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		    {
		        return $_SERVER["HTTP_X_FORWARDED_FOR"];
		    }
		    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
		    {
		        return $_SERVER["HTTP_X_FORWARDED"];
		    }
		    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
		    {
		        return $_SERVER["HTTP_FORWARDED_FOR"];
		    }
		    elseif (isset($_SERVER["HTTP_FORWARDED"]))
		    {
		        return $_SERVER["HTTP_FORWARDED"];
		    }
		    else
		    {
		        return $_SERVER["REMOTE_ADDR"];
		    }

		}

		public function elim_acentos($val){

			$val = trim($val);
			$val = str_replace("content-type:", "",$val);
	        $val = str_replace("mime-version:", "",$val);
	        $val = str_replace("content-transfer-encoding:", "",$val);
	        $val = str_replace("return-path:", "",$val);
			$val = str_replace("subject:", "",$val);
			$val = str_replace("from:", "",$val);
			$val = str_replace("envelope-to:", "",$val);
			$val = str_replace("to:", "",$val);
			$val = str_replace("bcc:", "",$val);
			$val = str_replace("cc:", "",$val);
			$val = str_replace("delete", "",$val);
			$val = str_replace("update", "",$val);
			$val = str_replace("drop", "",$val);
			$val = str_replace("dump", "",$val);
			$val = str_replace("copy", "",$val);   
			$val = str_replace("select", "",$val);
			$val = str_replace("while", "",$val);
			$val = str_replace("like", "",$val);
			$val = str_replace("and", "",$val);
			$val = str_replace("javascript", "",$val);
			$val = str_replace("script", "",$val);
			$val = str_replace("ScRiPt", "",$val);
			$val = str_replace("assign", "",$val);
			$val = str_replace("cleartimeout", "",$val);
			$val = str_replace("constructor", "",$val);
			$val = str_replace("encodeuri", "",$val);
			$val = str_replace("focus", "",$val);
			$val = str_replace("innerwidth", "",$val);
			$val = str_replace("mimetypes", "",$val);
			$val = str_replace("hidden", "",$val);
			$val = str_replace("open", "",$val);
			$val = str_replace("packages", "",$val);
			$val = str_replace("parseint", "",$val);
			$val = str_replace("propertyisenum", "",$val);
			$val = str_replace("scroll", "",$val);
			$val = str_replace("settimeout", "",$val);
			$val = str_replace("textarea", "",$val);
			$val = str_replace("blur", "",$val);
			$val = str_replace("clientinformation", "",$val);
			$val = str_replace("crypto", "",$val);
			$val = str_replace("encodeuricomponent", "",$val);
			$val = str_replace("layer", "",$val);
			$val = str_replace("navigate", "",$val);
			$val = str_replace("history", "",$val);
			$val = str_replace("opener", "",$val);
			$val = str_replace("pagexoffset", "",$val);
			$val = str_replace("password", "",$val);
			$val = str_replace("secure", "",$val);
			$val = str_replace("anchor", "",$val);
			$val = str_replace("button", "",$val);
			$val = str_replace("close", "",$val);
			$val = str_replace("decodeuri", "",$val);
			$val = str_replace("elements", "",$val);
			$val = str_replace("forms", "",$val);
			$val = str_replace("layers", "",$val);
			$val = str_replace("navigator", "",$val);
			$val = str_replace("option", "",$val);
			$val = str_replace("pageyoffset", "",$val);
			$val = str_replace("pkcs11", "",$val);
			$val = str_replace("reset", "",$val);
			$val = str_replace("select", "",$val);
			$val = str_replace("submit", "",$val);
			$val = str_replace("unescape", "",$val);
			$val = str_replace("anchors", "",$val);
			$val = str_replace("checkbox", "",$val);
			$val = str_replace("closed", "",$val);
			$val = str_replace("decodeuricomponent", "",$val);
			$val = str_replace("embed", "",$val);
			$val = str_replace("frame", "",$val);
			$val = str_replace("link", "",$val);
			$val = str_replace("frames", "",$val);
			$val = str_replace("images", "",$val);
			$val = str_replace("outerheight", "",$val);
			$val = str_replace("parent", "",$val);
			$val = str_replace("plugin", "",$val);
			$val = str_replace("screenx", "",$val);
			$val = str_replace("self", "",$val);
			$val = str_replace("taint", "",$val);
			$val = str_replace("untaint", "",$val);
			$val = str_replace("clearinterval", "",$val);
			$val = str_replace("defaultstatus", "",$val);
			$val = str_replace("embeds", "",$val);
			$val = str_replace("fileupload", "",$val);
			$val = str_replace("innerheight", "",$val);
			$val = str_replace("location", "",$val);
			$val = str_replace("framerate", "",$val);
			$val = str_replace("offscreenbuffering", "",$val);
			$val = str_replace("outerwidth", "",$val);
			$val = str_replace("parsefloat", "",$val);
			$val = str_replace("prompt", "",$val);
			$val = str_replace("screeny", "",$val);
			$val = str_replace("setinterval", "",$val);
			$val = str_replace("window", "",$val);
			$val = str_replace("onblur", "",$val);
			$val = str_replace("onkeydown", "",$val);
			$val = str_replace("onload", "",$val);
			$val = str_replace("onclick", "",$val);
			$val = str_replace("onkeypress", "",$val);
			$val = str_replace("onmouseup", "",$val);
			$val = str_replace("onerror", "",$val);
			$val = str_replace("onkeyup", "",$val);
			$val = str_replace("onmousedown", "",$val);
			$val = str_replace("onfocus", "",$val);
			$val = str_replace("onmouseover", "",$val);
			$val = str_replace("onsubmit", "",$val);
			$val = str_replace("getclass", "",$val);
			$val = str_replace("java", "",$val);
			$val = str_replace("javaarray", "",$val);
			$val = str_replace("javaclass", "",$val);
			$val = str_replace("javaobject", "",$val);
			$val = str_replace("javapackage", "",$val);
			$val = str_replace("array", "",$val);
			$val = str_replace("infinity", "",$val);
			$val = str_replace("math", "",$val);
			$val = str_replace("prototype", "",$val);
			$val = str_replace("date", "",$val);
			$val = str_replace("isfinite", "",$val);
			$val = str_replace("string", "",$val);
			$val = str_replace("isnan", "",$val);
			$val = str_replace("name", "",$val);
			$val = str_replace("tostring", "",$val);
			$val = str_replace("function", "",$val);
			$val = str_replace("isprototypeof", "",$val);
			$val = str_replace("number", "",$val);
			$val = str_replace("undefined", "",$val);
			$val = str_replace("hasownproperty", "",$val);
			$val = str_replace("length", "",$val);
			$val = str_replace("object", "",$val);
			$val = str_replace("valueof", "",$val);
			$val = str_replace("double", "",$val);
			$val = str_replace("extends", "",$val);
			$val = str_replace("import", "",$val);
			$val = str_replace("package", "",$val);
			$val = str_replace("short", "",$val);
			$val = str_replace("this", "",$val);
			$val = str_replace("try", "",$val);
			$val = str_replace("while", "",$val);
			$val = str_replace("arguments", "",$val);
			$val = str_replace("catch", "",$val);
			$val = str_replace("debugger", "",$val);
			$val = str_replace("else", "",$val);
			$val = str_replace("false", "",$val);
			$val = str_replace("function", "",$val);
			$val = str_replace("private", "",$val);
			$val = str_replace("static", "",$val);
			$val = str_replace("throw", "",$val);
			$val = str_replace("typeof", "",$val);
			$val = str_replace("with", "",$val);
			$val = str_replace("boolean", "",$val);
			$val = str_replace("default", "",$val);
			$val = str_replace("goto", "",$val);
			$val = str_replace("instanceof", "",$val);
			$val = str_replace("native", "",$val);
			$val = str_replace("protected", "",$val);
			$val = str_replace("throws", "",$val);
			$val = str_replace("yield", "",$val);
			$val = str_replace("break", "",$val);
			$val = str_replace("class", "",$val);
			$val = str_replace("delete", "",$val);
			$val = str_replace("finally", "",$val);
			$val = str_replace("new", "",$val);
			$val = str_replace("switch", "",$val);
			$val = str_replace("transient", "",$val);
			$val = str_replace("void", "",$val);
			$val = str_replace("byte", "",$val);
			$val = str_replace("float", "",$val);
			$val = str_replace("implements", "",$val);
			$val = str_replace("null", "",$val);
			$val = str_replace("return", "",$val);
			$val = str_replace("synchronized", "",$val);
			$val = str_replace("true", "",$val);
			$val = str_replace("volatile", "",$val);
			$val = str_replace("screen", "",$val);
			$val = str_replace("cookie", "",$val);
			$val = str_replace("getelementbyid", "",$val);
			$val = str_replace("/", "",$val);
			$val = str_replace("{", "",$val);
			$val = str_replace("}", "",$val);
			$val = str_replace("[", "",$val);
			$val = str_replace("]", "",$val);
			$val = str_replace("*", "",$val);
			$val = str_replace("%", "",$val);
			$val = str_replace("^", "",$val);
			$val = str_replace("--", "",$val);
			$val = str_replace("\"", "",$val);
			$val = str_replace("\\", "",$val);
			$val = str_replace("'", "",$val);
			$val = str_replace("‘", "",$val);
			$val = str_replace("’", "",$val);
			$val = str_replace("'", "",$val);
			$val = str_replace("&", "",$val);
			$val = str_replace("", "",$val);
			$val = str_replace("“", "",$val);
			$val = str_replace("”", "",$val);
			$val = str_replace("–", "",$val);
			$val = str_replace("–", "",$val);
			$val = str_replace("<", "",$val);
			$val = str_replace(">", "",$val);
			$val = str_replace("á", "a",$val);
			$val = str_replace("à", "a",$val);
			$val = str_replace("â", "a",$val);
			$val = str_replace("ã", "a",$val);
			$val = str_replace("é", "e",$val);
			$val = str_replace("è", "e",$val);
			$val = str_replace("ê", "e",$val);
			$val = str_replace("í", "i",$val);
			$val = str_replace("ì", "i",$val);
			$val = str_replace("î", "i",$val);
			$val = str_replace("ó", "o",$val);
			$val = str_replace("ò", "o",$val);
			$val = str_replace("ô", "o",$val);
			$val = str_replace("õ", "o",$val);
			$val = str_replace("ú", "u",$val);
			$val = str_replace("ù", "u",$val);
			$val = str_replace("û", "u",$val);
			$val = str_replace("Á", "a",$val);
			$val = str_replace("É", "e",$val);
			$val = str_replace("Í", "i",$val);
			$val = str_replace("Ó", "o",$val);
			$val = str_replace("Ú", "u",$val);
			$val = str_replace("ñ", "n",$val);
			$val = str_replace("Ñ", "n",$val);
			$val = str_replace("ç", "c",$val);
			$val = trim($val); 

			return $val;
		}
	}



?>