<?php namespace Controllers;

	use Models\Profile as Profile;
	use Controllers\validatorsController as validatorsController;
	


	class profilesController {

		private $validate;
		private $profile;
		private $usuario_md5 = "yoLeito";		
		
		public function __construct(){
			
			$this->profile = new Profile;
			$this->validate = new validatorsController;
			
		}


		
		public function usuarios($id){
			$this->profile->set("id", $id);
			$datos = $this->profile->listarUsers();
			return $datos;
		}

		public function index(){
			
			$datos="Bienvenido ".$_SESSION['usuario_nombre'];
			return $datos;
			
			
		}
		
		public function add(){

			if (!$_POST) {
				
				$datos=null;
				return $datos;
			}else{
				
				$this->profile->set("user", $_POST['user']);
				$this->profile->set("mail", $_POST['mail']);
				$this->profile->set("pass", $this->encripta($_POST['pass'], $this->usuario_md5));
				$this->profile->set("rol_id", $_POST['rol']);
				$this->profile->set("firts", 0);
				$this->profile->set("activo", 1);
				$this->profile->createUser();

				header("location: " . BASE_PATH . "index.php" );
			}
			
		}

		public function disable($id){
			if ($_SESSION['usuario_nivel'] == 2) {
				header("location: " . BASE_PATH . "logout.php?error=5");
			}
			$this->profile->set("id", $id);
			
			$this->profile->disableUser();
			
			header("Location: " . BASE_PATH . "index.php");
		}

		public function enable($id){
			if ($_SESSION['usuario_nivel'] == 2) {
				header("location: " . BASE_PATH . "logout.php?error=5");
			}
			$this->profile->set("id", $id);
			
			$this->profile->enableUser();

			header("Location: " . BASE_PATH . "index.php");
		}

		public function delete($id){
			if ($_SESSION['usuario_nivel'] == 2) {
				header("location: " . BASE_PATH . "logout.php?error=5");
			}
			$this->profile->set("id", $id);
			
			$this->profile->deleteUser();
			
			header("Location: " . BASE_PATH . "index.php");
		}

		public function edit($id){
			if ($_SESSION['usuario_nivel'] == 2) {
				header("location: " . BASE_PATH . "logout.php?error=5");
			}
			
			if (!$_POST) {
				$this->profile->set("id", $id);
				$datos = $this->profile->viewUser();
				return $datos;	
			}else{
				
				$this->profile->set("id", $id);
				$this->profile->set("user", $_POST['user']);
				$this->profile->set("mail", $_POST['mail']);
				
				$this->profile->set("rol_id", $_POST['rol']);
				
				
				$this->profile->editUser();

				header("location: " . BASE_PATH . "profiles/edit/".$id);
				

			}
			
		}

		public function reset($id){

			if ($_SESSION['usuario_nivel'] == 2) {
				header("location: " . BASE_PATH . "logout.php?error=5");
			}
			
			$this->profile->set("id", $id);
			
			$nkey = $this->validate->encripta($_POST['new_pass'], $this->usuario_md5);
			
			$this->profile->set("var", $nkey);
			
			$this->profile->resetKey();

			header("Location: " . BASE_PATH . "profiles/edit/".$id);
		}
		
		public function listarRoles(){
			$datos = $this->profile->roles();
			return $datos;
		}

		public function estadoKey($id){
			$this->profile->set("id", $id);
			$datos = $this->profile->estadoPass();
			return $datos;
		}

		public function usersDisabled(){
			$datos = $this->profile->allDisabled();
			return $datos;
		}

		public function verificaActivo($id){
			$this->profile->set("id", $id);
			$datos = $this->profile->verifyActive();
			return $datos;
		}

		public function encripta($pass, $usuarios_md5){
			$val1 = md5($pass.$usuarios_md5);
			$val = hash_hmac("sha512",$pass,$val1);
			return $val;
		}
	
	}






?>