FROM php:8-fpm-alpine

WORKDIR /var/www/html

RUN apk update && \
    apk add --update libxml2-dev \
                            curl \
                            git

RUN docker-php-ext-install mysqli pdo pdo_mysql soap opcache


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer


